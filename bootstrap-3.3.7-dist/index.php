
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- The above 2 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js\bootstrap.min.js""></script>
    <script type="text/javascript" src="js\Controller.js"></script>
    <script type="text/javascript">
      function bodyLoad(){
        checkSessionEtu();
      }
    </script>
    <title>Supofo</title>



    <link href="Styles/Etusivu.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body onload="bodyLoad()">
 
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
        <a class="navbar-brand" href="#">Supofo</a>
        </div>
        <div id="navbar">
          <ul class="nav navbar-nav">
            <li class="active Home"><a href="index.php">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="" data-toggle="modal" data-target="#logIn"> Login </a></li>
            <li><a href="" data-toggle="modal" data-target="#Register">Register</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">



      <h3 class="Teksti"> Share your portfolio with the world</h3>

      <!-- Register -->
      <div class="modal fade" id="Register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">Register</h4>
            </div> <!-- /.modal-header -->

            <div class="modal-body">
              <form role="form" action="common/register.php" method="post">

                <div class="form-group">
                  <div class="input-group">
                    <input type="text" class="form-control" id="uLogin" placeholder="UserName" name="username">
                    <label for="uLogin" class="input-group-addon glyphicon glyphicon-user"></label>
                  </div>
                </div> <!-- /.form-group -->

                <div class="form-group">
                  <div class="input-group">
                    <input type="Email" id="eMail" class="form-control" placeholder="Email" name="email">
                    <label for="eMail" class="input-group-addon glyphicon glyphicon-envelope"></label>
                  </div>
                </div>

                <div class="form-group">
                  <div class="input-group">
                    <input type="password" class="form-control" id="uPassword" placeholder="password" name="password">
                    <label for="uPassword" class="input-group-addon glyphicon glyphicon-lock"></label>
                  </div> <!-- /.input-group -->
                </div> <!-- /.form-group -->

                
                 <div class="form-group">
                  <input type="submit" value="submit" class="form-control bnt btn-primary">
                </div>
              </form>

            </div> <!-- /.modal-body -->
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <!-- Login -->
      <div class="modal fade" id="logIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">Log in</h4>
            </div> <!-- /.modal-header -->

            <div class="modal-body">
              <form role="form" action="common/login.php" method="post">
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" class="form-control" id="uLogin" placeholder="Login" name="username">
                    <label for="uLogin" class="input-group-addon glyphicon glyphicon-user"></label>
                  </div>
                </div> <!-- /.form-group -->

                <div class="form-group">
                  <div class="input-group">
                    <input type="password" class="form-control" id="uPassword" placeholder="Password" name="password">
                    <label for="uPassword" class="input-group-addon glyphicon glyphicon-lock"></label>
                  </div> <!-- /.input-group -->
                </div> <!-- /.form-group -->

                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Remember me
                  </label>
                </div> <!-- /.checkbox -->
                 <div class="form-group">
                  <input type="submit" value="submit" class="form-control bnt btn-primary">
                </div>
              </form>
              <a href="" data-toggle="modal" data-target="#forgot" data-dismiss="modal">forgot password?</a>
            </div> <!-- /.modal-body -->
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!-- /Login -->

          <!-- forgot -->
      <div class="modal fade" id="forgot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">Log in</h4>
            </div> <!-- /.modal-header -->

            <div class="modal-body">
              <form role="form" action="common/forgotpassword.php" method="post">
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" class="form-control" id="Email" placeholder="Email" name="email">
                    <label for="Email" class="input-group-addon glyphicon glyphicon-envelope"></label>
                  </div>

                </div> <!-- /.form-group -->
                 <div class="form-group">
                  <input type="submit" value="submit" class="form-control bnt btn-primary">
                </div>
              </form>

            </div> <!-- /.modal-body -->
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!-- /forgot -->
    </div> 
    <footer class="footer">
      <div class="container">
        <p class="text-muted footerText">This site use cookies  © 2017 Supofo </p>
      </div>
    </footer> 

    

  </body>
</html>
