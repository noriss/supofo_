<?php
session_start();
require_once('mysql.php');
$yhteys = mysqlConnect();
$username = $_POST["username"];

//HAETAAN MYSQL -SALASANA
$kysely = $yhteys->prepare("SELECT passwd FROM users WHERE uname = :username");
$kysely->execute(array('username' => $username));
$kantapassu = $kysely->fetch();
$verify = password_verify($_POST['password'],$kantapassu["passwd"]);
if ($verify)
{
$_SESSION["login"] = true;
$_SESSION["uname"] = $username;
header('Location: ../Kayttaja.php');
}
else
{
$_SESSION["login"] = false;

}
$yhteys = null;
?>
