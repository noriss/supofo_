﻿<?php
	//MYSQL CONNECTING
function mysqlConnect()
{
	//MYSQL CONFIG
	$db_host = "mysql.cc.puv.fi"; 
	$db_name = "e1400795_supofo"; //KANNAN NIMI
	$db_user = "e1400795";			 //KANNAN KÄYTTÄJÄ
	$db_pass = "";		 //KANNAN SALASANA

	//OPEN PDO
	try {
	$yhteys = new PDO("mysql:host=". $db_host .";dbname=" .$db_name, $db_user, $db_pass);
	} catch (PDOException $e) {
		die("VIRHE: " . $e->getMessage());
	}
	
	//ATTRIBUTES
	$yhteys->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$yhteys->exec("SET NAMES utf8");

	//RETURN
	return $yhteys;
}
?>